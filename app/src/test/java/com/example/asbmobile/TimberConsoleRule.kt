package com.example.asbmobile

import android.util.Log
import org.intellij.lang.annotations.Language
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class TimberConsoleRule : TestRule {
    override fun apply(base: Statement, description: Description) =
            TimberConsoleStatement(base)
}

class TimberConsoleStatement constructor(private val baseStatement: Statement) : Statement() {

    private val consoleTree = ConsoleTree()

    override fun evaluate() {
        Timber.plant(consoleTree)
        try {
            baseStatement.evaluate()
        } finally {
            Timber.uproot(consoleTree)
        }
    }
}

class ConsoleTree : Timber.DebugTree() {
    @Language("RegExp")
    private val anonymousClassPattern = Pattern.compile("""(\$\d+)+$""")

    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {
        val logTimeStamp = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault()).format(Date())
        val priorityChar = when (priority) {
            Log.VERBOSE -> 'V'
            Log.DEBUG -> 'D'
            Log.INFO -> 'I'
            Log.WARN -> 'W'
            Log.ERROR -> 'E'
            Log.ASSERT -> 'A'
            else -> '?'
        }

        println("$logTimeStamp $priorityChar/$tag: $message")
    }

    override fun createStackElementTag(element: StackTraceElement): String? {
        val matcher = anonymousClassPattern.matcher(element.className)
        val tag = when {
            matcher.find() -> matcher.replaceAll("")
            else -> element.className
        }
        return tag.substringAfterLast('.')
    }
}