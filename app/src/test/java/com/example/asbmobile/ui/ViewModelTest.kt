package com.example.asbmobile.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import com.example.asbmobile.CoroutineTestRule
import com.example.asbmobile.TimberConsoleRule
import org.junit.Rule


@ExperimentalCoroutinesApi
abstract class ViewModelTest {
    @get:Rule
    val loggingRule = TimberConsoleRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutineTestRule()
}