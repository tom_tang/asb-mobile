package com.example.asbmobile.ui.main

import com.example.asbmobile.*
import com.example.asbmobile.network.AsbApi
import com.example.asbmobile.network.Transaction
import com.example.asbmobile.ui.ViewModelTest
import com.example.asbmobile.ui.uniflow.data.ViewEvent
import com.example.asbmobile.ui.uniflow.data.ViewState
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verifySequence
import org.junit.Before
import org.junit.Test
import java.io.IOException
import java.math.BigDecimal

class MainViewModelTest : ViewModelTest() {
    private val asbApi: AsbApi = mockk()
    private lateinit var view: MockedViewObserver
    private val viewModel by lazy { MainViewModel(asbApi) }

    private val transactionDate = "2021-02-02T08:11:16+13:00".toDate()!!
    private val transaction1 = Transaction("1", transactionDate, "Summery 1", BigDecimal(1), BigDecimal(0))
    private val testTransactions = listOf(transaction1)

    @Before
    fun setup() {
        view = viewModel.mockObservers()
    }

    @Test
    fun `Success load transactions`() {
        coEvery { asbApi.getTransactions() } returns testTransactions
        viewModel.loadTransactions()
        val transactionItems = listOf(
            TransactionItem.TransactionHeader(transactionDate.format(DAY_MONTH_YEAR_FORMAT)),
            TransactionItem.TransactionBody(transaction1)
        )
        verifySequence {
            view.hasState(ViewState.Empty)
            view.hasState(ViewState.Loading)
            view.hasState(MainViewModel.MainViewState(transactionItems))
        }
    }

    @Test
    fun `Fail to load transactions`() {
        coEvery { asbApi.getTransactions() } throws IOException("No internet")
        viewModel.loadTransactions()
        verifySequence {
            view.hasState(ViewState.Empty)
            view.hasState(ViewState.Loading)
            view.hasState(ViewState.Failed)
            view.hasEvent(ViewEvent.ApiError("No internet"))
        }

    }
}