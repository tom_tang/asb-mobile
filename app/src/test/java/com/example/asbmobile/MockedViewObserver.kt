package com.example.asbmobile

import androidx.lifecycle.Observer
import com.example.asbmobile.ui.uniflow.DataFlowBaseViewModel
import com.example.asbmobile.ui.uniflow.data.Event
import com.example.asbmobile.ui.uniflow.data.ViewEvent
import com.example.asbmobile.ui.uniflow.data.ViewState
import io.mockk.mockk

data class MockedViewObserver(val states: Observer<ViewState>, val events: Observer<Event<ViewEvent>>) {
    infix fun hasState(state: ViewState) = states.onChanged(state)
    infix fun hasEvent(event: ViewEvent) = events.onChanged(Event(event))
}

fun DataFlowBaseViewModel.mockObservers(): MockedViewObserver {
    val viewStates: Observer<ViewState> = mockk(relaxed = true)
    val viewEvents: Observer<Event<ViewEvent>> = mockk(relaxed = true)
    dataPublisher.states.observeForever(viewStates)
    dataPublisher.events.observeForever(viewEvents)
    return MockedViewObserver(viewStates, viewEvents)
}