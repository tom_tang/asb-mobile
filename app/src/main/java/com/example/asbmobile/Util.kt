package com.example.asbmobile

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.text.SimpleDateFormat
import java.util.*

inline fun <reified VM : ViewModel> Fragment.provideViewModel(provider: ViewModelProvider.Factory): VM {
    return ViewModelProvider(this, provider).get(VM::class.java)
}

fun Date.format(format: String): String {
    val dateFormat = SimpleDateFormat(format, Locale.ENGLISH)
    return dateFormat.format(this)
}

fun String.toDate(format: String = API_DATE_FORMAT): Date? {
    val dateFormat = SimpleDateFormat(format, Locale.ENGLISH)
    return dateFormat.parse(this)
}

const val DAY_MONTH_YEAR_FORMAT = "d MMM yyyy"
const val FULL_DATE_FORMAT = "dd/MM/yyyy h:mm:ss a"
const val API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX"