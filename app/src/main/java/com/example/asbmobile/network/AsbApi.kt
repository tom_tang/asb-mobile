package com.example.asbmobile.network

import retrofit2.http.GET

interface AsbApi {
    @GET("v1/transactions")
    suspend fun getTransactions(): List<Transaction>
}