package com.example.asbmobile.network

import android.graphics.Color
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal
import java.util.*

@Parcelize
data class Transaction(
    val id: String,
    val transactionDate: Date,
    val summary: String,
    val debit: BigDecimal,
    val credit: BigDecimal
) : Parcelable {
    fun getAmount(): BigDecimal = if (debit > BigDecimal(0)) debit.negate() else credit
    fun getAmountColor() = if (debit > BigDecimal(0)) Color.RED else Color.GREEN
}