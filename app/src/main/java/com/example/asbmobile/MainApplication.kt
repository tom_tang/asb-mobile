package com.example.asbmobile

import android.app.Application
import com.example.asbmobile.di.AppComponent
import com.example.asbmobile.di.AppModule
import com.example.asbmobile.di.DaggerAppComponent
import timber.log.Timber

class MainApplication: Application() {

    val appComponent: AppComponent = DaggerAppComponent.builder()
        .appModule(AppModule(this))
        .build()

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}