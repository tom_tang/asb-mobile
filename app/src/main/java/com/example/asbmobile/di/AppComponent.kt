package com.example.asbmobile.di

import com.example.asbmobile.di.viewmodel.ViewModelModule
import com.example.asbmobile.ui.main.MainFragment
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [AppModule::class, ViewModelModule::class]
)
@Singleton
interface AppComponent {
    fun inject(into: MainFragment)
}