package com.example.asbmobile.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.asbmobile.MainApplication
import com.example.asbmobile.databinding.FragmentMainBinding
import com.example.asbmobile.network.Transaction
import com.example.asbmobile.provideViewModel
import com.example.asbmobile.ui.uniflow.data.ViewEvent
import com.example.asbmobile.ui.uniflow.data.ViewState
import com.example.asbmobile.ui.uniflow.onEvents
import com.example.asbmobile.ui.uniflow.onStates
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class MainFragment: Fragment(), TransactionViewHolder.TransactionCallback {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: MainViewModel

    private lateinit var binding: FragmentMainBinding
    private lateinit var transactionAdapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as MainApplication).appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        transactionAdapter = TransactionAdapter(this)
        binding.recyclerView.adapter = transactionAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.retry.setOnClickListener { viewModel.loadTransactions() }

        viewModel = provideViewModel(viewModelFactory)

        onStates(viewModel) { viewState ->
            when (viewState) {
                is ViewState.Loading -> renderLoading()
                is MainViewModel.MainViewState -> renderTransactionItems(viewState.transactionItems)
                is ViewState.Failed -> renderFailed()
            }
        }

        onEvents(viewModel) { viewEvent ->
            when (viewEvent) {
                is ViewEvent.ApiError -> Snackbar.make(view, viewEvent.message, Snackbar.LENGTH_LONG).show()
            }
        }

        viewModel.loadTransactions()
    }

    private fun renderTransactionItems(transactionItems: List<TransactionItem>) {
        binding.progressBar.isVisible = false
        binding.retry.isVisible = false
        transactionAdapter.setItems(transactionItems)
    }

    private fun renderLoading() {
        binding.progressBar.isVisible = true
        binding.retry.isVisible = false
    }

    private fun renderFailed() {
        binding.progressBar.isVisible = false
        binding.retry.isVisible = true
    }

    override fun onTransactionClicked(transaction: Transaction) {
        findNavController().navigate(MainFragmentDirections.actionMainFragmentToDetailFragment(transaction))
    }
}