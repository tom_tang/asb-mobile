package com.example.asbmobile.ui.main

import com.example.asbmobile.network.Transaction

sealed class TransactionItem {
    data class TransactionBody(val transaction: Transaction) : TransactionItem()
    data class TransactionHeader(val date: String) : TransactionItem()
}
