package com.example.asbmobile.ui.main

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import com.example.asbmobile.databinding.ItemTransactionBinding
import com.example.asbmobile.network.Transaction
import java.math.BigDecimal

class TransactionViewHolder(private val binding: ItemTransactionBinding, private val callback: TransactionCallback) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(body: TransactionItem.TransactionBody) {
        binding.summary.text = body.transaction.summary
        binding.amount.text = body.transaction.getAmount().toString()
        binding.amount.setTextColor(body.transaction.getAmountColor())
        binding.root.setOnClickListener { callback.onTransactionClicked(body.transaction) }
    }

    interface TransactionCallback {
        fun onTransactionClicked(transactionId: Transaction)
    }
}