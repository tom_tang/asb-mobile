package com.example.asbmobile.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.asbmobile.databinding.ItemTransactionBinding
import com.example.asbmobile.databinding.ItemTransactionDateBinding

class TransactionAdapter(private val callback: TransactionViewHolder.TransactionCallback) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: List<TransactionItem> = emptyList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_HEADER -> TransactionDateViewHolder(ItemTransactionDateBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            TYPE_BODY -> TransactionViewHolder(ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false), callback)
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is TransactionDateViewHolder -> holder.bind(items[position] as TransactionItem.TransactionHeader)
            is TransactionViewHolder -> holder.bind(items[position] as TransactionItem.TransactionBody)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is TransactionItem.TransactionBody -> TYPE_BODY
            is TransactionItem.TransactionHeader -> TYPE_HEADER
        }
    }

    fun setItems(transactionItems: List<TransactionItem>) {
        items = transactionItems
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_BODY = 1
    }
}
