package com.example.asbmobile.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.asbmobile.FULL_DATE_FORMAT
import com.example.asbmobile.databinding.FragmentDetailBinding
import com.example.asbmobile.format

class DetailFragment : Fragment() {
    private lateinit var binding: FragmentDetailBinding
    val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        args.transaction.run {
            binding.date.text = transactionDate.format(FULL_DATE_FORMAT)
            binding.summary.text = summary
            binding.amount.text = getAmount().toString()
            binding.amount.setTextColor(getAmountColor())
        }
    }
}