package com.example.asbmobile.ui.main

import androidx.recyclerview.widget.RecyclerView
import com.example.asbmobile.databinding.ItemTransactionDateBinding

class TransactionDateViewHolder(private val binding: ItemTransactionDateBinding) :  RecyclerView.ViewHolder(binding.root) {
    fun bind(transactionHeader: TransactionItem.TransactionHeader){
        binding.date.text = transactionHeader.date
    }
}