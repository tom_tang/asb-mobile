package com.example.asbmobile.ui.main

import com.example.asbmobile.DAY_MONTH_YEAR_FORMAT
import com.example.asbmobile.format
import com.example.asbmobile.network.AsbApi
import com.example.asbmobile.ui.uniflow.DataFlowBaseViewModel
import com.example.asbmobile.ui.uniflow.data.ViewEvent
import com.example.asbmobile.ui.uniflow.data.ViewState
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(private val asbApi: AsbApi) : DataFlowBaseViewModel() {
    fun loadTransactions() = action(
        onAction = {
            setState(ViewState.Loading)
            val transactions = asbApi.getTransactions()
            val transactionItems = arrayListOf<TransactionItem>()
            transactions.sortedByDescending { it.transactionDate }
                .groupBy { it.transactionDate.format(DAY_MONTH_YEAR_FORMAT) }
                .forEach {
                    transactionItems.add(TransactionItem.TransactionHeader(it.key))
                    transactionItems.addAll(it.value.map { TransactionItem.TransactionBody(it) })
                }
            setState(MainViewState(transactionItems))
        },
        onError = { error, _ ->
            Timber.e(error, "MainViewModel Error")
            setState(ViewState.Failed)
            sendEvent(ViewEvent.ApiError(error.message ?: "Unknown Error"))
        }

    )

    data class MainViewState(val transactionItems: List<TransactionItem>) : ViewState()
}